﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace SeleniumGoogleTask.PageObjects
{
    class SearchPage : BasicPage
    {
        public SearchPage(IWebDriver driver) : base(driver)
        {
        }

        //Maybe it be better to use CssSelector for consistance.
        private By searchInputSelector = By.Id("lst-ib");
        private By searchInputButton = By.Name("btnK");

        public SearchPage InputSearchQueue(string searchQueue)
        {
            InputText(searchInputSelector, searchQueue);
            ClosePopupHelp(searchInputSelector);
            return this;
        }

        public SearchPage ClickOnSearchButton()
        {
            Click(searchInputButton);
            return this;
        }
    }
}
