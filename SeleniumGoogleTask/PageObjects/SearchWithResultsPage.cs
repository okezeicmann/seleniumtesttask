﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using NUnit.Framework;

namespace SeleniumGoogleTask.PageObjects
{
    class SearchWithResultsPage : BasicPage
    {
        //This selects all refs to found pages. Unreadable selector.
        //I'd maybe change it for something else. :nth-child() doesn't work here
        //since page may contain more than 1 .srg elements.
        private By searchResultsLinksSelector = By.CssSelector(".srg > .g .r a");

        public SearchWithResultsPage(IWebDriver driver) : base(driver)
        {
        }

        public SearchWithResultsPage ClickSearchResultById(int resultId)
        {
            IList<IWebElement> searchResults = GetSearchResultsLinks();
            int resultsOnPage = searchResults.Count();
            if (resultsOnPage == 0) Assert.Fail("No search results presented");
            if (resultId > resultsOnPage)
                Assert.Fail($"No search result with index {resultId}, only {resultsOnPage} are presented");
            ClickLinkForcedSameTab(searchResults[resultId]);
            return this;
        }

        private IList<IWebElement> GetSearchResultsLinks()
        {
            return driver.FindElements(searchResultsLinksSelector);
        }
    }
}
