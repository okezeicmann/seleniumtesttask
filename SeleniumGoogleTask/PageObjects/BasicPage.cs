﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace SeleniumGoogleTask.PageObjects
{
    class BasicPage
    {
        protected IWebDriver driver;
        protected IWait<IWebDriver> wait;
        private const int timeOutValueSeconds = 5;

        public BasicPage(IWebDriver driver)
        {
            this.driver = driver;
            //TimeOut value should be moved to config.
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeOutValueSeconds));
        }

        protected void Click(By by)
        {
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(by));
            element.Click();
        }

        protected IWebElement InputText(By by, string text)
        {
            IWebElement element = wait.Until(ExpectedConditions.ElementIsVisible(by));
            element.Clear();
            element.SendKeys(text);
            return element;
        }

        protected IWebElement ClosePopupHelp(By by)
        {
            IWebElement element = wait.Until(ExpectedConditions.ElementIsVisible(by));
            //Requred to close popup which covers button elements;
            //This is usefult to have such methods for all kinds of popuping entities since
            //they make behavior less predictable.
            element.SendKeys(Keys.Escape);
            return element;
        }

        protected void ClickLinkForcedSameTab(IWebElement link)
        {
            //This is required in order to force browser open new page with target else
            //than _self on the same tab.
            //This is a hack but it has some advantages:
            //  - Works in all browsers;
            //  - Makes work consistance since there is no need to switch between tabs;
            //  - No probles with driver.quit() since only 1 tab is always open;
            ((IJavaScriptExecutor)driver)
                .ExecuteScript("arguments[0].setAttribute('target', '_self');", link);
            link.Click();
        }

        public string GetCurrentUrl()
        {
            return driver.Url;
        }
    }
}
