﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumGoogleTask.PageObjects;

namespace SeleniumGoogleTask
{
    class BasicTest
    {
        IWebDriver driver;
        //This should be in config also.
        private string baseUrl = "https://www.google.com";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(baseUrl);
        }

        [TearDown]
        public void Quit()
        {
            driver.Quit();
        }

        protected SearchPage AtSearchPage()
        {
            return new SearchPage(driver);
        }

        protected SearchWithResultsPage AtSearchResultsPage()
        {
            return new SearchWithResultsPage(driver);
        }

        //Added for compilator
        public static void Main(string[] args)
        {
        }
    }
}
