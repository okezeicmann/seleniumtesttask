﻿using NUnit.Framework;

namespace SeleniumGoogleTask
{
    class GoogleSearchTest : BasicTest
    {
        [Test]
        public void TestGoogleSearch()
        {
            //Takes parameters from tests.runsettings
            string query = TestContext.Parameters["searchWord"];
            string expectedUrl = TestContext.Parameters["endUrl"];
            //Search results starts with 0 index. So 3rd result has index 2;
            int desiredSearchResult = 2;

            AtSearchPage()
                .InputSearchQueue(query)
                .ClickOnSearchButton();

            string actualUrl = AtSearchResultsPage()
                .ClickSearchResultById(desiredSearchResult)
                .GetCurrentUrl();

            Assert.AreEqual(
                expectedUrl,
                actualUrl,
                "Unexpected Url is shown"
                );
        }
    }
}
